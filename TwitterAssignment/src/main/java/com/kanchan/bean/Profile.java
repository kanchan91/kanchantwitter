package com.kanchan.bean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kanchan.model.Follow;
import com.kanchan.model.Tweet;
import com.kanchan.model.User;
import com.kanchan.service.FollowService;
import com.kanchan.service.TweetService;
import com.kanchan.service.UserService;

@ManagedBean
@RequestScoped
@Component
public class Profile {

	@Autowired
	UserService userService;

	@Autowired
	private TweetService tweetService;

	@Autowired
	private FollowService followService;

	private User user;

	List<Tweet> userTwits;

	private boolean followed;
	
	private String message ="";
	
	private List<Follow> followers;
	private List<Follow> followees;

	public User getUser() {

		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String userProfile(String userName) {
		this.message ="";
		User LoggeedInUser = ((LoginBean) FacesContext
				.getCurrentInstance()
				.getApplication()
				.getELResolver()
				.getValue(FacesContext.getCurrentInstance().getELContext(),
						null, "loginBean")).getUser();
		// First get the user info
		if (userName != null && userName.length() > 0) {
			this.user = userService.getUserDetail(userName);
		} else {

			this.user = LoggeedInUser;
		}
		// check if loggedin user is following this user or not.
		List<Follow> followee = followService.getListOfFollower(LoggeedInUser);
		this.followed= false;
		if (followee != null && followee.size() > 0) {
			for (Follow fl : followee) {
				if (user.getUserId() == fl.getUserByFollowingIdFk().getUserId()) {
					followed = true;
					break;
				}
			}
		}

		this.userTwits = tweetService.getAllTweetList(user);

		return "profile";
	}
	
	public String followUser() {
		this.message = "";
		String value = FacesContext.getCurrentInstance().
				getExternalContext().getRequestParameterMap().get("userId");
		LoginBean loginBean =  (LoginBean) FacesContext
					.getCurrentInstance()
					.getApplication()
					.getELResolver()
					.getValue(FacesContext.getCurrentInstance().getELContext(),
							null, "loginBean");
		
		
		followService.followUser(Integer.valueOf(value), loginBean.getUser());
		this.message = IConstants.FOLLOW_MESSAGE;
		this.followed = true;

		return "profile";
	}

	public String unFollowUser() {
		this.message = "";
		String value = FacesContext.getCurrentInstance().
		getExternalContext().getRequestParameterMap().get("userId");
		LoginBean loginBean =  (LoginBean) FacesContext
					.getCurrentInstance()
					.getApplication()
					.getELResolver()
					.getValue(FacesContext.getCurrentInstance().getELContext(),
							null, "loginBean");
		
		followService.unFollowUser(Integer.valueOf(value), loginBean.getUser());
		this.message = IConstants.UNFOLLOW_MESSAGE;
		this.followed = false;
		return "profile";
	}


	public List<Tweet> getUserTwits() {
		return userTwits;
	}

	public void setUserTwits(List<Tweet> userTwits) {
		this.userTwits = userTwits;
	}

	public boolean isFollowed() {
		return followed;
	}

	public void setFollowed(boolean followed) {
		this.followed = followed;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Follow> getFollowers() {
		return followService.getListOfFollower(user);
	}

	public void setFollowers(List<Follow> followers) {
		this.followers = followers;
	}

	public List<Follow> getFollowees() {
		return followService.getListOfFollowing(user);
		
	}

	public void setFollowees(List<Follow> followees) {
		this.followees = followees;
	}


}
