package com.kanchan.service;

import java.util.List;

import com.kanchan.model.Tweet;
import com.kanchan.model.User;

public interface TweetService {
	public void tweet(User user, String tweet);
	public List<Tweet> getAllTweetList(User user);
	public List<Tweet> getAllTweetList();
	public List<Tweet> getAllTweetListForUser(User user);

}
