package com.kanchan.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.kanchan.model.Tweet;
import com.kanchan.service.TweetService;

@Component
@Scope("request")
public class TweetBean implements Serializable {

    @Autowired
    private TweetService tweetService;

    @ManagedProperty(value = "#{loginBean}")
    public LoginBean loginBean;

    private String tweet;

    private List<Tweet> tweets;

    public String getTweet() {
	return tweet;
    }

    public void setTweet(final String tweet) {
	this.tweet = tweet;
    }

    public List<Tweet> getTweets() {
	LoginBean lb = (LoginBean) FacesContext
		.getCurrentInstance()
		.getApplication()
		.getELResolver()
		.getValue(FacesContext.getCurrentInstance().getELContext(),
			null, "loginBean");
	if (lb != null && lb.getUser() != null) {
	    return tweetService.getAllTweetListForUser(lb.getUser());
	}
	return tweetService.getAllTweetList();
    }

    public void setTweets(final List<Tweet> tweets) {
	this.tweets = tweets;
    }

    public String postTweet() {
	if (this.loginBean == null) {
	    this.loginBean = (LoginBean) FacesContext
		    .getCurrentInstance()
		    .getApplication()
		    .getELResolver()
		    .getValue(FacesContext.getCurrentInstance().getELContext(),
			    null, "loginBean");
	}
	tweetService.tweet(loginBean.getUser(), tweet);
	this.tweet = "";
	this.tweets = tweetService.getAllTweetList(loginBean.getUser());
	return "success";
    }

    public void setLoginBean(final LoginBean loginBean) {
	this.loginBean = loginBean;
    }

    public LoginBean getLoginBean() {
	return this.loginBean;
    }

}
