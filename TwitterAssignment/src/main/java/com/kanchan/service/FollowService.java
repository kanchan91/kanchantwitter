package com.kanchan.service;

import java.util.List;

import com.kanchan.model.Follow;
import com.kanchan.model.User;

public interface FollowService {
	public int getNumberOfFollower(User user);
	public int getNumberOfFollowing(User user);
	
	public List<Follow> getListOfFollower(User user);
	public List<Follow> getListOfFollowing(User user);
	
	public void followUser(int toFollowUser, User followingUser);
	public void unFollowUser(int toUnFollowUser, User followingUser);

}
