package com.kanchan.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kanchan.dao.TweetDAO;
import com.kanchan.model.Tweet;
import com.kanchan.model.User;
import com.kanchan.service.TweetService;

@Transactional
@Service
public class TweetServiceImpl implements TweetService {
    @Autowired
    private TweetDAO tweetDAO;

    @Override
    public void tweet(final User user, final String tweet) {
	tweetDAO.tweet(user, tweet);

    }

    @Override
    public List<Tweet> getAllTweetList(final User user) {
	return tweetDAO.getAllTweetList(user);
    }

    @Override
    public List<Tweet> getAllTweetList() {
	return tweetDAO.getAllTweetList();
    }

    @Override
    public List<Tweet> getAllTweetListForUser(final User user) {
	return tweetDAO.getAllTweetListForUser(user);
    }

}
