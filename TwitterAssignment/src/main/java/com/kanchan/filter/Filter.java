package com.kanchan.filter;

import java.io.IOException;

import javax.faces.application.Application;
import javax.faces.context.FacesContext;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.primefaces.context.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import com.kanchan.bean.LoginBean;


public class Filter implements javax.servlet.Filter {

	private static String login = "/index.xhtml";

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		if (request instanceof HttpServletRequest
				&& response instanceof HttpServletResponse) {
			HttpSession session = ((HttpServletRequest)request).getSession();
			
			LoginBean loginBean =  (LoginBean) session.getAttribute("loginBean");
			
			String uri = ((HttpServletRequest) request).getRequestURI();
			if ((loginBean != null && loginBean.isLogin())
					|| uri.contains("index") || uri.contains("resource") || uri.contains("register") ) {
				chain.doFilter(request, response);
			} else {

				((HttpServletResponse) response)
						.sendRedirect(((HttpServletRequest) request)
								.getContextPath() + login);
			}
		}

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
