package com.kanchan.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.kanchan.dao.RegisterDAO;
import com.kanchan.util.Encrypt;

@Component
@Scope("request")
public class RegisterBean {
    @Autowired
    private RegisterDAO register;

    private String firstName;
    private String lastName;
    private String username;
    private String email;
    private String password;
    private String message;

    public void setEmail(final String email) {
	this.email = email;
    }

    public String getFirstName() {
	return firstName;
    }

    public void setFirstName(final String firstName) {
	this.firstName = firstName;
    }

    public String getLastName() {
	return lastName;
    }

    public void setLastName(final String lastName) {
	this.lastName = lastName;
    }

    public String getUsername() {
	return username;
    }

    public void setUsername(final String username) {
	this.username = username;
    }

    public String getEmail() {
	return email;
    }

    public void setEmailId(final String email) {
	this.email = email;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(final String password) {
	this.password = password;
    }

    public String getMessage() {
	return message;
    }

    public void setMessage(final String message) {
	this.message = message;
    }

    public String registerUser() {
	if (!register.doesUserExist(firstName, lastName, email, username,
		Encrypt.encryptPassword(password))) {
	    return "register success";
	}
	this.message = "User already exists. Please use another user name.";
	return "register fail";

    }
}
