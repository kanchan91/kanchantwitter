package com.kanchan.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kanchan.dao.FollowDAO;
import com.kanchan.model.Follow;
import com.kanchan.model.User;
import com.kanchan.service.FollowService;

@Transactional
@Service
public class FollowServiceImpl implements FollowService, Serializable {

    @Autowired
    private FollowDAO followDAO;

    @Override
    public int getNumberOfFollower(final User user) {
	return followDAO.getNumberOfFollower(user);
    }

    @Override
    public int getNumberOfFollowing(final User user) {
	return followDAO.getNumberOfFollowing(user);
    }

    @Override
    public List<Follow> getListOfFollower(final User user) {
	return followDAO.getListOfFollower(user);
    }

    @Override
    public List<Follow> getListOfFollowing(final User user) {
	return followDAO.getListOfFollowing(user);
    }

    @Override
    public void followUser(final int toFollowUser, final User followingUser) {
	followDAO.followUser(toFollowUser, followingUser);

    }

    @Override
    public void unFollowUser(final int toUnFollowUser, final User followingUser) {
	followDAO.unFollowUser(toUnFollowUser, followingUser);

    }

}
