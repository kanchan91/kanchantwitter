package com.kanchan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kanchan.dao.UserDAO;
import com.kanchan.model.User;
import com.kanchan.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Override
    public User validateUser(final String username, final String password) {

	return userDAO.validateLogin(username, password);
    }

    @Override
    public User getUserDetail(final String username) {
	return userDAO.getUserDetail(username);
    }

    @Override
    public List<User> getAllUsers() {
	return userDAO.getAllUsers();
    }

}
