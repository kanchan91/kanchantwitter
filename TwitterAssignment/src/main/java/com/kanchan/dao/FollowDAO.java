package com.kanchan.dao;

import java.util.List;

import com.kanchan.model.Follow;
import com.kanchan.model.User;

public interface FollowDAO {
	
	public void followUser(int toFollowUser, User followingUser);
	public void unFollowUser(int toUnFollowUser, User followingUser);
	
	public int getNumberOfFollower(User user);
	public int getNumberOfFollowing(User user);
	
	public List<Follow> getListOfFollower(User user);
	public List<Follow> getListOfFollowing(User user);
	

}
