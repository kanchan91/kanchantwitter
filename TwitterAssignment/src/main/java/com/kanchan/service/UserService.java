package com.kanchan.service;

import java.util.List;

import com.kanchan.model.User;

public interface UserService {
	public User validateUser(String username, String password);
	
	public User getUserDetail(String username);
	
	public List<User> getAllUsers();
	

}
