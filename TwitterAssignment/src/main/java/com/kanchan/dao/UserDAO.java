package com.kanchan.dao;

import java.util.List;

import com.kanchan.model.User;

public interface UserDAO {

	public User validateLogin(String username, String password);
	public User getUserDetail(String username);
	public List<User> getAllUsers();
}
