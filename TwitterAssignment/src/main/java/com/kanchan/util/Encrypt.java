package com.kanchan.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Encrypt {

    public static String encryptPassword(final String password) {
	StringBuffer sb = new StringBuffer();
	try {
	    MessageDigest md = MessageDigest.getInstance("MD5");
	    md.update(password.getBytes());

	    byte byteData[] = md.digest();

	    for (byte element : byteData) {
		sb.append(Integer.toString((element & 0xff) + 0x100, 16)
			.substring(1));
	    }
	} catch (NoSuchAlgorithmException e) {
	    e.printStackTrace();
	}

	return sb.toString();
    }

}
