package com.kanchan.dao;

public interface RegisterDAO {
	
	public boolean doesUserExist(String firstName, String lastName, String email, String username, String password);

}
