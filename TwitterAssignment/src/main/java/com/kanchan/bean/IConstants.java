package com.kanchan.bean;

public interface IConstants {
	public static final String LOGIN_ERROR_MESSAGE ="Please provide valid username and password";
	public static final String FOLLOW_MESSAGE = "You are now following ";
	public static final String UNFOLLOW_MESSAGE = "You stopped following ";
	
	public static final String LOGOUT_MESSAGE = "You are out of Cook Systems Twitter. Please log in to enjoy. ";

}
