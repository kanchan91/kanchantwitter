package com.kanchan.dao;

import java.util.List;

import com.kanchan.model.Tweet;
import com.kanchan.model.User;

public interface TweetDAO {
	
	public void tweet(User user, String tweet);
	public List<Tweet> getAllTweetList(User user);
	public List<Tweet> getAllTweetList();
	public List<Tweet> getAllTweetListForUser(User user);
	

}
