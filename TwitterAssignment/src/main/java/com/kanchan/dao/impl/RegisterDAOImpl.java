package com.kanchan.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kanchan.dao.RegisterDAO;
import com.kanchan.model.User;

@Repository
@Transactional
public class RegisterDAOImpl implements RegisterDAO{
	@Autowired
	private SessionFactory factory;

	@Override
	public boolean doesUserExist(String firstName, String lastName, String email, String username, String password) {
		if(isUserUnique(username)){
		 User user =new User();
		 user.setFirstName(firstName);
		 user.setLastName(lastName);
		 user.setEmail(email);
		 user.setUserName(username);
		 user.setPassword(password);
		 
		 Session session =factory.getCurrentSession();
		 session.save(user);
		 return false;
		}
		 return true;
		 
	}

	public boolean isUserUnique(String userName) {
		Session session =factory.getCurrentSession();
		List<User> userList = new ArrayList<User>();
		userList = session.createQuery("from User").list();
		for(User user : userList){
			if(user.getUserName().equals(userName)){
				return false;
			}
			
		}
		return true;
	}

}
