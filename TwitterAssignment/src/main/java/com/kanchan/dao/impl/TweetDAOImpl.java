package com.kanchan.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kanchan.dao.TweetDAO;
import com.kanchan.model.Tweet;
import com.kanchan.model.User;

@Repository
@Transactional
public class TweetDAOImpl implements TweetDAO {

    @Autowired
    private SessionFactory factory;

    @Override
    public void tweet(final User user, final String tweet) {
	Session session = factory.getCurrentSession();
	Query query = session
		.createQuery("from User where userName = :username");
	query.setString("username", user.getUserName());
	List<User> users = query.list();
	User usr = null;
	if (users != null && users.size() == 1) {
	    usr = users.get(0);
	    if (usr != null) {
		Tweet twt = new Tweet();
		twt.setTweet(tweet);
		twt.setTimeCretaed(new Date(System.currentTimeMillis()));
		twt.setUser(usr);
		session.save(twt);
	    }
	}
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Tweet> getAllTweetList(final User user) {

	Session session = factory.getCurrentSession();

	Query query = session
		.createQuery("from Tweet where user = :user order by timeCretaed desc");
	query.setEntity("user", user);
	List<Tweet> tweetList = query.list();

	return tweetList;

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Tweet> getAllTweetList() {

	Session session = factory.getCurrentSession();

	Query query = session
		.createQuery("from Tweet order by timeCretaed desc");
	List<Tweet> tweetList = query.list();

	return tweetList;

    }

    @Override
    public List<Tweet> getAllTweetListForUser(final User user) {
	Session session = factory.getCurrentSession();

	Query query = session
		.createSQLQuery(
			"select * from Tweet where user_id= :user OR user_id in(select following_id_fk  from Follow where follower_id_fk = :user and active=1 ) order by time_Cretaed desc")
		.addEntity(Tweet.class);
	query.setInteger("user", user.getUserId());
	List<Tweet> tweetList = query.list();

	return tweetList;
    }

}
