package com.kanchan.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kanchan.dao.FollowDAO;
import com.kanchan.model.Follow;
import com.kanchan.model.FollowId;
import com.kanchan.model.User;

@Transactional
@Repository
public class FollowDAOImpl implements FollowDAO, Serializable {
    @Autowired
    private SessionFactory factory;

    @Override
    public int getNumberOfFollower(final User user) {

	Session session = factory.getCurrentSession();
	Query query = session
		.createQuery("from Follow f where f.followerIdFK = :user and active = 1");
	query.setInteger("user", user.getUserId());
	List<User> users = query.list();
	return users.size();
    }

    @Override
    public int getNumberOfFollowing(final User user) {
	Query query = factory.getCurrentSession().createQuery(
		"from Follow f where f.followingIdFK =:user and active = 1");
	query.setInteger("user", user.getUserId());
	return query.list().size();
    }

    @Override
    public List<Follow> getListOfFollower(final User user) {
	Session session = factory.getCurrentSession();
	Query query = session
		.createQuery("from Follow where userByFollowerIdFk =:user and active = 1");
	query.setEntity("user", user);
	List<Follow> followers = query.list();
	return followers;
    }

    @Override
    public List<Follow> getListOfFollowing(final User user) {
	Session session = factory.getCurrentSession();
	Query query = session
		.createQuery("from Follow where userByFollowingIdFk =:user and active = 1");
	query.setInteger("user", user.getUserId());
	List<Follow> followee = query.list();
	return followee;
    }

    @Override
    public void followUser(final int toFollowUser, final User followingUser) {
	Session session = factory.getCurrentSession();
	Query query = session
		.createQuery("from Follow where follower_id_fk = :follower and following_id_fk=:following");
	query.setInteger("following", toFollowUser);
	query.setInteger("follower", followingUser.getUserId());
	List<Follow> followList = query.list();
	if (followList != null && followList.size() == 1) {
	    Follow follow = followList.get(0);
	    follow.setActive(true);
	    session.update(follow);
	} else {
	    Follow follow = new Follow();
	    FollowId followId = new FollowId();
	    followId.setFollowerIdFk(followingUser.getUserId());
	    followId.setFollowingIdFk(toFollowUser);
	    follow.setActive(true);
	    follow.setId(followId);
	    session.save(follow);
	}
    }

    @Override
    public void unFollowUser(final int toUnFollowUser, final User followingUser) {
	Query query = factory
		.getCurrentSession()
		.createQuery(
			"update Follow set active = 0 where follower_id_fk = :follower and following_id_fk=:following");
	query.setInteger("follower", followingUser.getUserId());
	query.setInteger("following", toUnFollowUser);

	query.executeUpdate();
	factory.getCurrentSession().flush();
    }

}
