package com.kanchan.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kanchan.dao.UserDAO;
import com.kanchan.model.User;
@Repository
@Transactional
public class UserDAOImpl implements UserDAO {

	@Autowired
	private SessionFactory factory;
	
	@SuppressWarnings("unchecked")
	@Override
	public User validateLogin(String username, String password) {
		Query query = factory.getCurrentSession().createQuery("from User where userName = :username and password = :password");
		query.setString("username", username);
		query.setString("password", password);
		List<User> users = query.list();
		User user = null;
		if(users != null && users.size() ==1){
			user = users.get(0);
			
		}
		return user;
	}
	
	public User getUserDetail(String username) {
		Query query = factory.getCurrentSession().createQuery("from User where userName = :username");
		query.setString("username", username);
		
		List<User> users = query.list();
		User user = null;
		if(users != null && users.size() ==1){
			user = users.get(0);
			
		}
		return user;
	}

	@Override
	public List<User> getAllUsers() {
		Query query = factory.getCurrentSession().createQuery("from User");
		
		return query.list();
	}
	

}
