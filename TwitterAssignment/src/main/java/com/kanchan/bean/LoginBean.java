package com.kanchan.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.kanchan.model.Follow;
import com.kanchan.model.Tweet;
import com.kanchan.model.User;
import com.kanchan.service.FollowService;
import com.kanchan.service.TweetService;
import com.kanchan.service.UserService;
import com.kanchan.util.Encrypt;

@Component
@Scope("session")
public class LoginBean implements Serializable {

    private static final Logger LOGGER = LoggerFactory
	    .getLogger(LoginBean.class);

    @Autowired
    private UserService userService;

    @Autowired
    private FollowService followService;

    @Autowired
    private TweetService tweetService;

    private String username = null;
    private String password = null;
    private String errorMessage;
    private List<Follow> followers;
    private List<Follow> followees;
    private List<Tweet> tweets;

    private User user;

    public String getUsername() {
	return username;
    }

    public void setUsername(final String username) {
	this.username = username;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(final String password) {
	this.password = password;
    }

    public User getUser() {
	return user;
    }

    public void setUser(final User user) {
	this.user = user;
    }

    public String getErrorMessage() {
	return errorMessage;
    }

    public void setErrorMessage(final String errorMessage) {
	this.errorMessage = errorMessage;
    }

    public String login() {
	LOGGER.info("Login started for " + username);
	// encrypt the password and call the user service
	User user = userService.validateUser(username,
		Encrypt.encryptPassword(password));
	if (user != null) {
	    this.user = user;
	    FacesContext.getCurrentInstance().getExternalContext()
		    .getSessionMap().put("loginBean", this);
	    LOGGER.info("Login Succesful for " + username);

	    return "success";
	} else {
	    this.errorMessage = IConstants.LOGIN_ERROR_MESSAGE;
	    this.username = null;
	    this.password = null;
	    return "failure";

	}
    }

    public boolean isLogin() {
	return username != null && password != null;
    }

    public String register() {
	this.username = null;
	this.password = null;
	this.user = null;
	return "register";
    }

    public String logout() {
	this.username = null;
	this.password = null;
	this.user = null;
	HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
		.getExternalContext().getSession(false);
	session.invalidate();
	this.errorMessage = IConstants.LOGOUT_MESSAGE;
	return "logout";
    }

    public List<Follow> getFollowers() {
	return followService.getListOfFollower(user);
    }

    public void setFollowers(final List<Follow> followers) {
	this.followers = followers;
    }

    public List<Follow> getFollowees() {
	return followService.getListOfFollowing(user);

    }

    public void setFollowees(final List<Follow> followees) {
	this.followees = followees;
    }

    public List<Tweet> getTweets() {
	return tweetService.getAllTweetList(user);
    }

    public void setTweets(final List<Tweet> tweets) {
	this.tweets = tweets;
    }

}
