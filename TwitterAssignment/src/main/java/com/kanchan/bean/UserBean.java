package com.kanchan.bean;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.kanchan.model.User;
import com.kanchan.service.UserService;

@Component
@Scope("request")
public class UserBean {

    @Autowired
    private UserService userService;

    List<User> allUsers;

    public List<User> getAllUsers() {
	return userService.getAllUsers();
    }

    public void setAllUsers(final List<User> user) {
	this.allUsers = user;
    }

}
